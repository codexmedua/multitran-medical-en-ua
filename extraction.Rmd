---
title: "R Notebook"
output: html_notebook
---

```{r}
library(rvest)
library(stringr)
```

```{r}
lien_suivant <- "https://www.multitran.com/m.exe?a=110&l1=1&l2=33&letter=a&sc=8"
selector <- "#start > div:nth-child(7) > table:nth-child(34) > tbody"
```

```{r}
iter = 0
while (TRUE)
{
  print(paste0("itération: ", iter))
  print(lien_suivant)
  
  brute <- rvest::read_html(lien_suivant)
  
  
  
  donnees_extraites <- brute %>%
    html_elements("table") %>% html_table() %>% tail(n = 1)
  
  donnees_extraites %>% write.table("traduction_extraites.csv",
                                    append = TRUE,
                                    col.names = FALSE)
  
  lien_suivant_selector <- brute %>%
    html_elements("div") %>%
    html_elements("div") %>%
    html_elements("a")
  
  print(lien_suivant_selector %>% html_attr("href"))
  
  lien_suivant <-
    paste0("https://www.multitran.com",
           lien_suivant_selector[lien_suivant_selector %>% html_text() == ">>"] %>%
             html_attr("href"))
  
  Sys.sleep(2)
  
  iter = iter + 1
  
  lien_suivant_selector <- NA
  brute <- NA
}
```



